// ************ ABC COMMENT XHR REQUEST ****************


function ABCChange() {
        var usrn = document.getElementById("username").value; 
        var newComment = document.getElementById("ABCcomment").value;
        var column = "ABCcomment";
        var url = "commentProcess.php";
        var params = 'usrn=' + usrn + "&newComment=" + newComment + "&column=" + column;
        
        
        var xhr = new XMLHttpRequest();

        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            
          alert("Your changes have been saved");            

          }
        }
        
        xhr.send(params);
        
        return target;
      }

  // ************ XYZ COMMENT XHR REQUEST ****************

  function XYZChange() {
        var usrn = document.getElementById("username").value; 
        var newComment = document.getElementById("XYZcomment").value;
        
        var column = "XYZcomment";
        var url = "commentProcess.php";
        var params = 'usrn=' + usrn + "&newComment=" + newComment + "&column=" + column;
        
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            
              alert("Your changes have been saved");      

          }
        }
        
        xhr.send(params);
        
        return target;
      }

  // ************ ACME COMMENT XHR REQUEST ****************

  function AcmeChange() {
        var usrn = document.getElementById("username").value; 
        var newComment = document.getElementById("Acmecomment").value;
        
        var column = "Acmecomment";
        var url = "commentProcess.php";
        var params = 'usrn=' + usrn + "&newComment=" + newComment + "&column=" + column;
        
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            
            alert("Your changes have been saved");  
                  

          }
        }
        
        xhr.send(params);
        
        return target;
      }

  // ************ Fling COMMENT XHR REQUEST ****************

  function FlingChange() {
        var usrn = document.getElementById("username").value; 
        var newComment = document.getElementById("Flingcomment").value;
        
        var column = "Flingcomment";
        var url = "commentProcess.php";
        var params = 'usrn=' + usrn + "&newComment=" + newComment + "&column=" + column;
        
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            
            alert("Your changes have been saved");  
                  

          }
        }
        
        xhr.send(params);
        
        return target;
      }

  // ************ Neutral COMMENT XHR REQUEST ****************

  function NeutralChange() {
        var usrn = document.getElementById("username").value; 
        var newComment = document.getElementById("Neutralcomment").value;
        
        var column = "Neutralcomment";
        var url = "commentProcess.php";
        var params = 'usrn=' + usrn + "&newComment=" + newComment + "&column=" + column;
        
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            
                  alert("Your changes have been saved");  

          }
        }
        
        xhr.send(params);
        
        return target;
      }


  // ************ TSI COMMENT XHR REQUEST ****************

  function TSIChange() {
        var usrn = document.getElementById("username").value; 
        var newComment = document.getElementById("TSIcomment").value;
        
        var column = "TSIcomment";
        var url = "commentProcess.php";
        var params = 'usrn=' + usrn + "&newComment=" + newComment + "&column=" + column;
        
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
        xhr.onreadystatechange = function () {
          console.log('readyState: ' + xhr.readyState);
          if(xhr.readyState == 2) {
            target.innerHTML = 'Loading...';
          }
          if(xhr.readyState == 4 && xhr.status == 200) {
            
                  alert("Your changes have been saved");  

          }
        }
        
        xhr.send(params);
        
        return target;
      }